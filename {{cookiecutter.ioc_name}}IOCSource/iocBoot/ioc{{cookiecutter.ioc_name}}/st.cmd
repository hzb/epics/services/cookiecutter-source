#!../../bin/linux-x86_64/{{cookiecutter.ioc_name}}

< envPaths
epicsEnvSet("ENGINEER","{{cookiecutter.engineer}}")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/{{cookiecutter.ioc_name}}.dbd"
{{cookiecutter.ioc_name}}_registerRecordDeviceDriver pdbbase

## Load record instances

#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/pvs.dbl
